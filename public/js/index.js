const visibleButtons = document.querySelectorAll(".visible-password-btn");
const invisibleButtons = document.querySelectorAll(".invisible-password-btn");
const copyButtons = document.querySelectorAll(".copy-btn");

invisibleButtons.forEach(element => {
    element.addEventListener('click', togglePassword);
});

visibleButtons.forEach(element => {
    element.addEventListener('click', togglePassword);
});

copyButtons.forEach(element => {
    element.addEventListener('click', copyPassword);
});

function togglePassword(evt) {
    passwordRow = evt.target.parentElement.parentElement.parentElement;

    const invisiblePasswordElement = passwordRow.childNodes[1].childNodes[1];
    const invisiblePasswordButton = evt.target.parentElement.childNodes[1];

    const visiblePasswordElement = passwordRow.childNodes[1].childNodes[3];
    const visiblePasswordButton = evt.target.parentElement.childNodes[3];

    invisiblePasswordElement.style.display = invisiblePasswordElement.style.display == "none"? "block" : "none";
    visiblePasswordElement.style.display = visiblePasswordElement.style.display == "none"? "block" : "none";

    invisiblePasswordButton.style.display = invisiblePasswordButton.style.display == "none"? "block" : "none";
    visiblePasswordButton.style.display = visiblePasswordButton.style.display == "none"? "block" : "none";
}

function copyPassword(evt) {
    passwordRow = evt.target.parentElement.parentElement;

    const passwordElement = passwordRow.childNodes[1].childNodes[3];
    console.log(navigator.clipboard);
    navigator.clipboard.writeText(passwordElement.innerHTML);
}

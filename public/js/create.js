const copyButton = document.querySelector("#copy-btn");

const passwordElement = document.querySelector("#password");

const passLength = document.querySelector("#length");
const passLengthLabel = document.querySelector("#length-label");
const complexityLabel = document.querySelector("#complexity-label");

const background = document.querySelector("#background");

passLength.addEventListener("input", updateLengthLabel)
passLengthLabel.innerHTML = `Password Length: ${passLength.value}`;

complexity.addEventListener("input", updateComplexityLabel)
complexityLabel.innerHTML = `Password Complexity: ${complexity.value}`;

checkPasswordStrength();

copyButton.addEventListener('click', copyPassword);

function copyPassword() {
    if(passwordElement.value !== "") {
        navigator.clipboard.writeText(passwordElement.value);
    }
}

function updateLengthLabel() {
    passLengthLabel.innerHTML = `Password Length: ${passLength.value}`;
    checkPasswordStrength();
}

function updateComplexityLabel() {
    complexityLabel.innerHTML = `Password Complexity: ${complexity.value}`
    checkPasswordStrength();
}

function checkPasswordStrength() {
    let strength = passLength.value * complexity.value;
    if(strength >= 8 && strength <= 50) {
        background.style.backgroundColor = "#fa6e6e";
    }   else if(strength > 50 && strength <= 90) {
        background.style.backgroundColor = "#c9886d";
    }   else if(strength > 90 && strength <= 130) {
        background.style.backgroundColor = "#98a36b";
    }   else if(strength > 130 && strength <= 170) {
        background.style.backgroundColor = "#67bd6a";
    }   else if(strength > 170 && strength <= 210) {
        background.style.backgroundColor = "#36d869";
    }   else if(strength > 210 && strength <= 250) {
        background.style.backgroundColor = "#1de568";
    }
}

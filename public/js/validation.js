const title = document.querySelector("#passtitle");
const description = document.querySelector("#passdescription-text-field");
const passlength = document.querySelector("#length");
const complexity = document.querySelector("#complexity");

const generatePasswordBtn = document.querySelector("#generate-password-btn");

const password = document.querySelector("#password");
const hiddenTitle = document.querySelector("#title");
const hiddenDescription = document.querySelector("#description");

const saveBtn = document.querySelector("#save");

let titleValidated = false;
let descriptionValidated = false;

title.addEventListener('keydown', validateTitle);
title.addEventListener('blur', renderTitleErrorMessage);
description.addEventListener('keydown', validateDescription);
description.addEventListener('blur', renderDescriptionErrorMessage);
generatePasswordBtn.addEventListener('click', generateNewPassword);

function validateTitle() {
    let value = title.value;
    if(value.length > 2){
        titleValidated = true;
        document.querySelector("#title_error").innerHTML = "";
    }   else    {
        titleValidated = false;
    }
    updateGenerateButton();
}

function renderTitleErrorMessage() {
    console.log("Here");
    if(! titleValidated) {
        document.querySelector("#title_error").innerHTML = "Title must have more than 2 characters";
    }
}

function validateDescription() {
    let value = description.value;
    if(value.length > 12){
        descriptionValidated = true;
        document.querySelector("#description_error").innerHTML = "";
    }   else    {
        descriptionValidated = false;
    }
    updateGenerateButton();
}

function renderDescriptionErrorMessage() {
    if(! descriptionValidated) {
        document.querySelector("#description_error").innerHTML = "Description must have more than 2 characters";
    }
}

function updateGenerateButton() {
    if(titleValidated && descriptionValidated) {
        generatePasswordBtn.className = "btn btn-dark";
    }   else    {
        generatePasswordBtn.className = "btn btn-dark disabled";
    }
}

function generateNewPassword(evt) {
    evt.preventDefault();
    if(titleValidated && descriptionValidated){
        const pass = new Password(passlength.value, complexity.value);
        const passwordText = pass.generatePassword();

        password.value = passwordText;
        hiddenTitle.value = title.value;
        hiddenDescription.value = description.value;

        saveBtn.className = "btn btn-dark btn-lg";
    }
}

class Password {
    static characters = "abcdefghijklmnopqrstuvwxyz";
    static lowerCase = Password.characters.split('');
    static upperCase = Password.characters.toUpperCase().split('');
    static numbers = "1234567890".split('');
    static specialChars = "!\"#$%&'()*+,-./:;<=>?@[\]^_`{|}~".split('');
    constructor(length, complexity) {
        this.length = length;
        this.complexity = complexity;
    }

    setWeights() {
        this.wtList = [];
        if(this.complexity == 1) {
            this.pushData(Math.round(Math.random() * 3) + 3, 'l');
            this.pushData(Math.round(Math.random() * 1) + 2, 'u');
            this.pushData(Math.round(Math.random() * 1) + 0, 'n');
        }   else if(this.complexity == 2) {
            this.pushData(Math.round(Math.random() * 2) + 2, 'l');
            this.pushData(Math.round(Math.random() * 2) + 2, 'u');
            this.pushData(Math.round(Math.random() * 1) + 1, 'n');
            this.pushData(Math.round(Math.random() * 1) + 0, 's');
        }   else if(this.complexity == 3) {
            this.pushData(Math.round(Math.random() * 1) + 2, 'l');
            this.pushData(Math.round(Math.random() * 1) + 1, 'u');
            this.pushData(Math.round(Math.random() * 2) + 1, 'n');
            this.pushData(Math.round(Math.random() * 1) + 1, 's');
        }   else if(this.complexity == 4) {
            this.pushData(Math.round(Math.random() * 1) + 1, 'l');
            this.pushData(Math.round(Math.random() * 1) + 1, 'u');
            this.pushData(Math.round(Math.random() * 3) + 2, 'n');
            this.pushData(Math.round(Math.random() * 2) + 1, 's');
        }   else if(this.complexity == 5) {
            this.pushData(Math.round(Math.random() * 1) + 1, 'l');
            this.pushData(Math.round(Math.random() * 1) + 1, 'u');
            this.pushData(Math.round(Math.random() * 1) + 2, 'n');
            this.pushData(Math.round(Math.random() * 2) + 1, 's');
        }
    }

    pushData(amount, type) {
        for(let i = 0; i < amount; i++){
            this.wtList.push(type);
        }
    }

    generatePassword() {
        this.setWeights();
        let ch = '';
        this.password = "";
        for(let i = 0; i < this.length; i++) {
            ch = this.wtList[this.random(this.wtList)];
            switch(ch){
                case 'l':
                    this.password += Password.lowerCase[this.random(Password.lowerCase)];
                    break;
                case 'u':
                    this.password += Password.upperCase[this.random(Password.upperCase)];
                    break;
                case 'n':
                    this.password += Password.numbers[this.random(Password.numbers)];
                    break;
                case 's':
                    this.password += Password.specialChars[this.random(Password.specialChars)];
                    break;
            }
        }
        return this.password;
    }

    random(list) {
        return Math.floor(Math.random() * list.length)
    }
}

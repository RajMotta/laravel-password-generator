<?php

namespace App\Http\Controllers;

use App\Models\Password;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class PasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $passwords = auth()->user()->passwords;

        return view('passwords.index', compact(['passwords']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('passwords.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Password::create([
            'user_id' => auth()->id(),
            'title' => $request->title,
            'description' => $request->description,
            'enc_password' => Crypt::encryptString($request->enc_password)
        ]);
        User::find(auth()->id())->increment('saved_count');
        return redirect(route('passwords.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Password $password
     * @return \Illuminate\Http\Response
     */
    public function destroy(Password $password)
    {
        $password->delete();
        User::find(auth()->id())->decrement('saved_count');
        return redirect()->back();
    }
}

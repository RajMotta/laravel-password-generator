<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Password extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'enc_password'
    ];

    public function getDecryptedPasswordAttribute() {
        return Crypt::decryptString($this->enc_password);
    }

    /**
     * Relationship Methods
     */

    public function owner() {
        return $this->belongsTo(User::class, 'user_id');
    }
}

<?php

use App\Http\Controllers\PasswordController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function() {
    Route::get('passwords/', [PasswordController::class, 'index'])->name('passwords.index');
    Route::post('passwords/create', [PasswordController::class, 'store'])->name('password.store');
    Route::delete('passwords/{password}', [PasswordController::class, 'destroy'])->name('password.delete');
});

Route::get('passwords/create', [PasswordController::class, 'create'])->name('passwords.create');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

@extends('passwords.layout.app')

@section('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.css" />
@endsection

@section('content')
<div id="set-password-params">
    <div class="row">
        <div class="col-12 mt-4">
            <h2 class="text-light">Set Password Parameters</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form id="password-params-form">
                <div class="form-group mt-3">
                    <label for="passtitle" class="form-label text-light fs-5">Password Title</label>
                    <input type="text" class="form-control" name="passtitle" id="passtitle">
                    <small id="title_error" class="text-danger"></small>
                </div>
                <div class="form-group mt-2">
                    <label for="passdescription" class="form-label text-light fs-5">Password Description</label>
                    <input type="hidden" class="form-control" name="passdescription" id="passdescription"/>
                    <trix-editor input="passdescription" placeholder="Password Description" class="form-control" id="passdescription-text-field"></trix-editor>
                    <small id="description_error" class="text-danger"></small>
                </div>
                <div class="row mt-3 d-flex justify-content-around">
                    <div class="col-5">
                        <div class="form-group">
                            <label id="length-label" for="length" class="form-label text-light fs-5">Password Length:</label>
                            <input type="range" class="form-range" name="length" id="length" min="8" max="50" step="1" style="--bs-form-range-thumb-bg: #333">
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="form-group">
                            <label id="complexity-label" for="complexity" class="form-label text-light fs-5">Password Complexity:</label>
                            <input type="range" class="form-range" name="complexity" id="complexity" min="1" max="5" step="1" style="--bs-form-range-thumb-bg: #333">
                        </div>
                    </div>
                </div>
                <div class="form-group mt-3">
                    <button id="generate-password-btn" type="submit" class="btn btn-dark disabled" style="--bs-bg-opacity: 1;">Generate New Password</button>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form action="{{ route('password.store')}}" method="POST" class="password-display-form mt-4">
                @csrf
                <div class="row d-flex align-items-center">
                    <div class="col-10">
                        <div class="row d-flex align-items-center">
                            <div class="col-11">
                                <div class="form-group d-flex flex-column">
                                    <label for="enc_password" class="form-label text-light fs-5">Password</label>
                                    <input type="text" class="form-group border-0 border-bottom border-2 border-light bg-transparent text-light fs-3 fw-semibold" name="enc_password" id="password" readonly>
                                </div>
                            </div>
                            <div class="col-1 text-light d-flex justify-content-center">
                                <i id="copy-btn" class="fa-solid fa-copy fs-2"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-2 d-flex align-items-center justify-content-center">
                        <div class="form-group">
                            <button type="submit" class="btn btn-dark btn-lg disabled" name="save" id="save">Save Password</button>
                        </div>
                    </div>
                    <input type="text" name="title" id="title" hidden>
                    <input type="text" name="description" id="description" hidden>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.3.1/trix.js"></script>
<script src="{{ asset("js/validation.js") }}"></script>
<script src="{{ asset("js/create.js") }}"></script>
@endsection

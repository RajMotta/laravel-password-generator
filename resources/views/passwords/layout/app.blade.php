<!DOCTYPE html>
<html lang="en">
@include('passwords.layout.partials._head')
<body>
    @include('passwords.layout.partials._navbar')
    <div id="background" style="min-height: 82.6vh; --bs-bg-opacity: 0.8; background-color: rgba(var(--bs-dark-rgb), var(--bs-bg-opacity))">
        <div class="container pb-5">
            @yield('content')
        </div>
    </div>
    @include('passwords.layout.partials._footer')
</body>
@include('passwords.layout.partials._scripts')
@yield('scripts')
</html>

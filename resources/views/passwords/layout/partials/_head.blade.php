<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', "Password Generator")</title>
    @include('passwords.layout.partials._styles')
    @yield('styles')
</head>

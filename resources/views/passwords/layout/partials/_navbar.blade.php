<nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-dark text-light" style="--bs-bg-opacity: 0.95">
    <div class="container-fluid d-flex justify-content-between">
        <div class="navbar-interactives d-flex">
            <a class="navbar-brand text-light fw-semibold" href="#">Password Generator</a>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link text-light" aria-current="page" href="{{ route('passwords.create') }}">Generate Password</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-light @auth @else disabled @endauth" href="{{ route('passwords.index') }}">View Passwords</a>
                    </li>
                </ul>
            </div>
        </div>
        @auth
            <div class="navbar-user-details me-5 d-flex align-items-center">
                <img src="{{ auth()->user()->avatar }}" style="height: 30px">
                <p class="m-0 ms-2 fs-5">{{ auth()->user()->name }}</p>
            </div>
        @else
            <div class="navbar-login-options me-5 dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Login Options
                </a>
                <ul class="dropdown-menu justify-content-end" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="{{ route('login') }}">Log In</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item" href="{{ route('register')}} ">Sign Up</a></li>
                </ul>
            </div>
        @endauth
    </div>
</nav>

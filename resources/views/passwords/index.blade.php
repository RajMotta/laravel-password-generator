@extends('passwords.layout.app')

@section('content')
<div id="user-info">
    <div class="row">
        <div class="col-12 mt-4">
            <div class="user-details">
                <h2 class="fs-4 text-white fw-semibold d-flex align-items-center">
                    <img src="{{ auth()->user()->avatar }}" style="height:60px"/>
                    <p class="m-0 ms-2">{{ auth()->user()->name }}</p>
                </h2>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div id="passwords-generated-count" class="col-6 d-flex justify-content-center">
            <p class="fs-5 text-white">Passwords Generated: {{ auth()->user()->password_count }}</p>
        </div>
        <div id="passwords-saved-count" class="col-6 d-flex justify-content-center">
            <p class="fs-5 text-white">Passwords Saved: {{ auth()->user()->saved_count }}</p>
        </div>
    </div>
</div>
<div id="user-passwords">
    <div class="row mt-4">
        <div class="col-12">
            @if($passwords->isNotEmpty())
                <table class="table table-dark table-striped" width="100%">
                    <thead>
                        <tr class="fs-5">
                            <th>Id</th>
                            <th>Description</th>
                            <th>Password</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($passwords as $index => $password)
                        <tr>
                            <td>{{ $password->title }}</td>
                            <td>{!! $password->description !!}</td>
                            <td id="password-{{$index}}" class="d-flex justify-content-between">
                                <div class="password">
                                    <p class="m-0 invisible-password">
                                        @for ($i = 0; $i < 10; $i++)
                                            <small><i class="fa-solid fa-circle"></i></small>
                                        @endfor
                                    </p>
                                    <p class="m-0 visible-password" style="display: none;">{{ $password->decrypted_password }}</p>
                                </div>
                                <div class="actions d-flex align-items-center">
                                    <div class="view-password-actions">
                                        <i class="fa-solid fa-eye invisible-password-btn"></i>
                                        <i class="fa-solid fa-eye-slash visible-password-btn" style="display: none;"></i>
                                    </div>
                                    <i class="fa-solid fa-copy ms-3 copy-btn"></i>
                                    <form action="{{ route('password.delete', $password) }}" method="POST">
                                        @csrf
                                        @method("DELETE")
                                        <button type="submit" class="border-0 bg-transparent text-light">
                                            <i class="fa-solid fa-trash ms-2"></i>
                                        </button>

                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <div class="d-flex align-items-center flex-column">
                    <h3 class="text-light fs-4 fw-bold">You have not saved a password yet!</h3>
                    <div class="mt-3">
                        <a href="{{ route('passwords.create') }}" class="btn btn-success text-light fs-5">Generate your first password!</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{asset("js/index.js")}}"></script>
@endsection

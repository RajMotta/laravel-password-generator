<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Password;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if(App::environment() === 'production') { exit(); }
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        // 1. Truncate All Tables
        $tables = DB::select('SHOW TABLES');
        $attributeName = "Tables_in_".env("DB_DATABASE");

        foreach($tables as $table) {
            if($table->$attributeName !== 'migrations'){
                DB::table($table->$attributeName)->truncate();
            }
        }

        // 2. Seed the tables with new data
        $userCount = 10;
        $passwordCount = 50;

        User::factory()->count($userCount)->create();

        Password::factory()->count($passwordCount)->create();
    }
}

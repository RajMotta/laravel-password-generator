<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Crypt;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PasswordFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $user = User::get()->random();
        $user->increment('password_count');
        $user->increment('saved_count');
        $userId = $user->id;
        return [
            'user_id' => $userId,
            'title' => $this->faker->word(),
            'description' => $this->faker->sentence($this->faker->randomNumber(1)),
            'enc_password' => Crypt::encryptString($this->faker->word())
        ];
    }
}
